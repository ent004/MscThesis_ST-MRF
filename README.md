# MscThesis_ST-MRF

code and data used for my Master thesis with Space-Time Markov Random Fields.

The code was written in Rstudio v3.3.2. with the following package distributions:

rgdal 1.2-18

raster 2.6-7

randomForest 4.6-14

sp 1.2.7
